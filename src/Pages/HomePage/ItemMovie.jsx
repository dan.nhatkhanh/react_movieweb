import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";

const { Meta } = Card;
export default function ItemMovie({ data }) {
  let { hinhAnh, tenPhim, maPhim } = data;

  return (
    <div>
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img
            className="h-80 w-full object-cover"
            alt={tenPhim}
            src={hinhAnh}
          />
        }
      >
        <Meta
          title={<p className="text-red-500 truncate">{tenPhim}</p>}
          description="www.instagram.com"
        />

        <NavLink to={`/detail/${maPhim}`}>
          <button className="w-full py-2 bg-red-500 text-white mt-5 rounded transition duration-300 hover:bg-black">
            Xem chi tiết
          </button>
        </NavLink>
      </Card>
    </div>
  );
}
