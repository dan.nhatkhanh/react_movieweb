import React, { useEffect } from "react";
import { useState } from "react";
import { moviesServ } from "../../services/movieService";
import CarouselMovie from "./CarouselMovie";
import ItemMovie from "./ItemMovie";
import TabsMovie from "./TabsMovie";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    moviesServ
      .getListMovie()
      .then((res) => {
        console.log(res);
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderMovies = () => {
    return movies.map((movie) => {
      return <ItemMovie key={movie.maPhim} data={movie} />;
    });
  };

  return (
    <div id="lichchieu">
      <CarouselMovie />
      <div className="container mx-auto relative">
        <div className="grid grid-cols-5 gap-4">{renderMovies()}</div>
        <TabsMovie />
      </div>
    </div>
  );
}
