import React, { useEffect } from "react";
import { useState } from "react";
import { moviesServ } from "../../services/movieService";
import { Tabs } from "antd";
import ItemTabsMovie from "./ItemTabsMovie";

export default function TabsMovie() {
  const [dataMovies, setDataMovies] = useState([]);

  useEffect(() => {
    moviesServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderContent = () => {
    return dataMovies.map((heThongRap) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={heThongRap.maHeThongRap}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={cumRap.maCumRap}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="h-32 scrollbar-thin scrollbar-thumb-gray-900 scrollbar-track-gray-100"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabsMovie key={phim.maPhim} data={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div id="cumrap" className="shadow">
      <div className="my-10">
        {" "}
        <h2 className="text-center text-5xl font-bold">CỤM RẠP</h2>
        <Tabs defaultActiveKey="1" tabPosition="left" style={{ height: 500 }}>
          {renderContent()}
        </Tabs>
      </div>
    </div>
  );
}
