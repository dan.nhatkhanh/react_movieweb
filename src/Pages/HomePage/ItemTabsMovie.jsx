import React from "react";
import moment from "moment";

export default function ItemTabsMovie({ data }) {
  return (
    <div className="p-3 flex space-x-5 border-b border-red-500">
      <img
        className="w-28 h-36 object-cover"
        src={data.hinhAnh}
        alt={data.tenPhim}
      />

      <div className="flex-grow">
        <p>{data.tenPhim}</p>

        <div className="grid grid-cols-3 gap-5">
          {data.lstLichChieuTheoPhim.map((gioChieu) => {
            return (
              <div
                key={gioChieu.maLichChieu}
                className="p-3 rounded bg-red-500 text-white"
              >
                {moment(gioChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm"
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
