import React from "react";
import { Button, Form, Input, message } from "antd";
import { userServ } from "../../services/userService";
import { localServ } from "../../services/localService";
import { useDispatch } from "react-redux";
import { SET_USER } from "../../redux/constants/constantUser/constantUser";
import { useNavigate } from "react-router-dom";
import bg_animate from "../../assets/bg_login.json";
import Lottie from "lottie-react";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        localServ.user.set(res.data.content);

        dispatch({
          type: SET_USER,
          payload: res.data.content,
        });

        message.success("Đăng nhập thành công");
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="container mx-auto h-screen w-screen flex justify-center items-center">
      <div className="w-1/2 h-full">
        <Lottie animationData={bg_animate} />
      </div>
      <div className="w-1/2 h-full flex items-center justify-center">
        <Form
          className="w-full bg-blue-300 p-10"
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{}}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tài khoản!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
