import React from "react";
import Header from "../Components/HeaderTheme/Header";

export default function layout({ Component }) {
  return (
    <div className="">
      <Header />
      <div className="">{<Component />}</div>
    </div>
  );
}
