import Layout from "./Layout/Layout";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import DetailPage from "./Pages/DetailPage/DetailPage";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage.jsx/LoginPage";
import "antd/dist/antd.css";
import "flowbite";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
