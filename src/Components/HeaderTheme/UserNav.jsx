import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogout = () => {
    localServ.user.remove();
    window.location.href = "/login";
  };

  let renderUserStatue = () => {
    if (user) {
      return (
        <>
          <span className="text-red-700 underline">{user.hoTen}</span>
          <NavLink to="/login">
            <button
              onClick={() => {
                handleLogout();
              }}
              className="border rounded-2xl text-red-500 border-red-500 hover:bg-red-500 hover:text-white px-5 py-1.5 transition duration-300 font-semibold"
            >
              Đăng xuất
            </button>
          </NavLink>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="border rounded-2xl text-green-700 border-green-700 hover:bg-green-700 hover:text-white px-5 py-1.5 transition duration-300 font-semibold">
              Đăng nhập
            </button>
          </NavLink>
          <button className="border rounded-2xl text-gray-500 border-gray-500 hover:bg-gray-500 hover:text-white px-5 py-1.5 transition duration-300 font-semibold">
            Đăng ký
          </button>
        </>
      );
    }
  };

  return <div className="space-x-5">{renderUserStatue()}</div>;
}
