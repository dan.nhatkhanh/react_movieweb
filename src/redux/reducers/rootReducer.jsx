import { combineReducers } from "redux";
import { userReducer } from "./userReducer";

export const rootReducer_User = combineReducers({
  userReducer,
});
